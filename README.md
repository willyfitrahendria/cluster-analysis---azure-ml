# Cluster Analysis - Azure ML

The aim of this project is to explore cluster analysis in Microsoft Azure Machine Learning

---
In Progress

Problem
---
A customer informed their consultant that they have developed several formulations of petrol that gives different characteristics of burning pattern. The formulations are obtaining by adding varying levels of additives that, for example, prevent engine knocking, gum prevention, stability in storage, and etc. However, a third party certification organisation would like to verify if the formulations are significantly different, and request for both physical and statistical proof. Since the formulations are confidential information, they are not named in the dataset.
Please assist the consultant in the area of statistical analysis